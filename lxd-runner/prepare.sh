#!/bin/bash
##
set -eu

PROGBASE=$(d=$(dirname -- "${0}"); cd "${d}" && pwd)
. "${PROGBASE}/base.inc"

# trap any error, and mark it as a system failure.
trap "exit ${SYSTEM_FAILURE_EXIT_CODE}" ERR


printf 'Running in %s\n' "${CONTAINER_ID}"

if lxc info "${CONTAINER_ID}" >/dev/null 2>&1; then
	printf 'Found old container, deleting\n'
	lxc delete --force "${CONTAINER_ID}"
fi

#
# see https://gitlab.com/gitlab-org/gitlab-runner/issues/4357
#
lxc launch 'ubuntu:18.04' "${CONTAINER_ID}"

for i in $(seq 1 10); do
	if lxc ls --format=csv -cs "${CONTAINER_ID}" | grep -q RUNNING; then
		# give a chance to the network to be brought up
		sleep 5
		break
	fi

	if [ X10 = X"${i}" ]; then
		printf 'Waited for 10 seconds to start container, exiting...\n'
		exit "${SYSTEM_FAILURE_EXIT_CODE}"
	fi
	sleep 1
done


install_deps() {
	cat <<__EOF__
# Install Git LFS, git comes pre installed with ubuntu image.
curl -s https://packagecloud.io/install/repositories/github/git-lfs/script.deb.sh | sudo bash
apt-get -y install git-lfs

# Install gitlab-runner binary since we need for cache/artifacts.
curl -L --output /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64
chmod +x /usr/local/bin/gitlab-runner
__EOF__
}

install_deps | lxc exec "${CONTAINER_ID}" -- /bin/sh -s
