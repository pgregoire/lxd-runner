#!/bin/sh
##

PROGBASE=$(d=$(dirname -- "${0}"); cd "${d}" && pwd)
. "${PROGBASE}/base.inc"

lxc exec "$CONTAINER_ID" /bin/bash < "${1}"
if [ X0 != X"${?}" ]; then
	# Exit using the variable, to make the build as failure in GitLab CI.
	exit "${BUILD_FAILURE_EXIT_CODE}"
fi
