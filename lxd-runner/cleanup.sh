#!/bin/sh
##
set -eu

PROGBASE=$(d=$(dirname -- "${0}"); cd "${d}" && pwd)
. "${PROGBASE}/base.inc"

printf 'Deleting container %s\n' "${CONTAINER_ID}"

lxc delete --force "${CONTAINER_ID}"
