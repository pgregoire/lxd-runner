# lxd-runner

Provides a set of scripts to use LXD with GitLab runners.


## Installation

`sudo cp -R /path/to/repo/lxd-runner /opt/`


## Registration

Below is an example registration session. We set the `lxd` tag 

```
# gitlab-runner register
Runtime platform                                    arch=amd64 os=linux pid=562 revision=a987417a version=12.2.0
Running in system-mode.

Please enter the gitlab-ci coordinator URL (e.g. https://gitlab.com/):
https://gitlab.com/
Please enter the gitlab-ci token for this runner:
(token)
Please enter the gitlab-ci description for this runner:
[hostname]: hostname
Please enter the gitlab-ci tags for this runner (comma separated):
lxd
Registering runner... succeeded                     runner=2mEcLqgw
Please enter the executor: docker-ssh+machine, custom, docker, docker-ssh, parallels, shell, virtualbox, docker+machine, ssh, kubernetes:
custom
Runner registered successfully. Feel free to start it, but if it's running already the config should be automatically reloaded!
```

Finally, your runner should have a config that looks like the below,
in `/etc/gitlab-runner/config.toml`.

```
[[runners]]
  name = "hostname"
  url = "https://gitlab.com/"
  token = "secret token"
  executor = "custom"
  builds_dir = "/builds"
  cache_dir = "/cache"
  [runners.custom]
    prepare_exec = "/opt/lxd-runner/prepare.sh"
    run_exec = "/opt/lxd-runner/run.sh"
    cleanup_exec = "/opt/lxd-runner/cleanup.sh"
```


## Usage

In your `gitlab-ci.yml` file, set the `lxd` tag with the job you want
to have running in a LXD container.

```
myjob:
  tags:
    - lxd
  script:
    - echo "Hello, World"
```


## Credits

With minor changes, these scripts originate from
[here](https://docs.gitlab.com/runner/executors/custom_examples/lxd.html)
